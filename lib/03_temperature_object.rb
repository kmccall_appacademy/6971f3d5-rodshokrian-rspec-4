class Temperature

  def self.from_celsius(cent)
    Temperature.new(c: cent)
  end

  def self.from_fahrenheit(fahr)
    Temperature.new(f: fahr)
  end

  def initialize(options = {})
    @c = options[:c]
    @f = options[:f]
  end

  def fahrenheit
    @f
  end

  def celsius
    @c
  end

  def in_fahrenheit
    celsius ? celsius * 1.8 + 32 : fahrenheit
  end

  def in_celsius
    fahrenheit ? ((fahrenheit - 32) / 1.8).round : celsius
  end

end
