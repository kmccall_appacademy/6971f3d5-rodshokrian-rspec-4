
class Dictionary

  def initialize
    @dict_hash = {}
  end

  def entries
    @dict_hash
  end

  def keywords
    entries.keys.sort
  end

  def add(entry)
    if entry.class == String
      entries[entry] = nil
    else
      entries.merge!(entry)
    end
  end

  def include?(entry)
    entries.keys.include?(entry)
  end

  def find(word)
    result = {}
    entries.each {|entry, definition| result[entry] = definition if entry.include?(word)}
    result
  end

  def printable
    entries.sort.each {|k, v| puts "#{k} #{v}"}
  end
end
