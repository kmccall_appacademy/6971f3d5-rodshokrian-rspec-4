class Book

  def title
    lower_case_list = ["the", "a", "an", "and", "in", "of"]
    @raw_title.split(" ").map.with_index {|word, idx| !lower_case_list.include?(word) || idx == 0 ? word.capitalize : word}.join(" ")
  end

  def title=(raw_title)
    @raw_title = raw_title
  end
end
