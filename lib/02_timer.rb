class Timer

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def seconds
    @seconds
  end

  def seconds=(secs)
    @seconds = secs
  end

  def time_string
    results = []
    total = @seconds
    while total != 0
      results << total % 60
      total = total / 60
    end
    results << 0 while results.length < 3
    results.map! {|i| self.padder(i)}
    "#{results[2]}:#{results[1]}:#{results[0]}"
  end

  def padder(time)
    time < 10 ? "0#{time}" : time.to_s
  end

end